<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <script src="js/jquery.dev.js"></script>
    <script src="js/soundmanager2.js"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/floating-button.css">
    <link rel="stylesheet" href="css/radios.css">
    <link rel="stylesheet" href="css/custom-svg-icons.css">

    <title>Radios.com</title>
<script>
soundManager.setup({
  url: '/swf/',
  flashVersion: 9, // optional: shiny features (default = 8)
  // optional: ignore Flash where possible, use 100% HTML5 mode
  preferFlash: false,
});
</script>
  </head>
  <body>
  <div class = "fixedElement">
    
    <h1>Radios.com! 

    <button type="button" class="btn btn-default" onclick = "RadioTestAll()"><span class = "glyphicon glyphicon-play"></span></button>

    <button type="button" class="btn btn-default" onclick = "radio_generador()"><span class = "glyphicon glyphicon-list-alt"></span></button>

    </h1>
    

    <div>
      <ul class="nav nav-tabs" id = "country-list">
      <?php include 'db/country-select.php'; ?>
        
      </ul>
    </div>

   </div>

  <div class="container">

      <div id = "radio-list">

          <form class="form-inline go-bottom">
          <div class="form-group">
            <label for="input1">Nombre</label>
            <input type="text" class="form-control" id="inputName-0" placeholder="Radio NombrE" value = "Radio Satelite">
          </div>
          <div class="form-group">
            <label for="input2-0">URL</label>
            <input type="text" class="form-control" id="inputUrl-0" placeholder="http://url.url" value = "https://ice42.securenetsystems.net/SATELITE">
          </div>
          <button type="button" class="btn btn-default"><span class = "glyphicon glyphicon-pencil"></span></button>
          <button type="button" class="btn btn-default"><span class = "glyphicon glyphicon-globe"></span></button>
          <button type="button" class="btn btn-default"><span class = "glyphicon glyphicon-globe"></span></button>
          <button type="button" class="btn btn-default manual-check-button"><span class = "glyphicon glyphicon-eye-open"></span></button>

          <span id = "status-0"></span>
          </form>

      </div>

  </div>

  


  <div id="floating-button" data-toggle="tooltip" data-placement="left" data-original-title="Abrir/Cerrar Chat" onclick="openModal_radioNueva()">
    <p class="plus"><span class = "glyphicon glyphicon-plus"></span></p>
    
  </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

    <script src = "js/radios.js"></script>
    <script src="js/bootstrap.js"></script>
    
  </body>
</html>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Agregar Radio</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="inputNombre">Radio Nombre</label>
            <input type="email" class="form-control" id="inputNombre" placeholder="Radio nombre">
          </div>
          <div class="form-group">
            <label for="inputUrl">URL</label>
            <input type="text" class="form-control" id="inputUrl" placeholder="http://url.url">
          </div>
          <div class="form-group">
            <label for="inputImagen">Imagen</label>
            <input type="text" class="form-control" id="inputImagen" placeholder="emi01">
          </div>
          <div class="form-group">
            <label for="inputWeb">Web</label>
            <input type="text" class="form-control" id="inputWeb" placeholder="radio.com">
          </div>
          <div class="form-group">
            <label for="inputNotas">Notas</label>
            <textarea type="text" col = 3 class="form-control" id="inputNotas" placeholder="Tus notas aqui"></textarea> 
          </div>

          <div id = "last-update"></div>


          
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button id = "botonModal0" type="button" class="btn btn-primary" onclick = "radio_guardar()">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModal0" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel0">Agregar Grupo de Radios</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="inputNombreGrupo">Nombre del Grupo</label>
            <input type="email" class="form-control" id="inputNombreGrupo" placeholder="Radio nombre">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" onclick = "radio_grupoGuardar()">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel1">Autogenerar Código</h4>
      </div>
      <div class="modal-body" id = "form-generar">
        <select class="form-control" id = "selector-cuenta">
          <option>Chinoko</option>
          <option>Pao</option>
          <option>Jair</option>
          <option>Caleta.tk</option>
        </select>
        <div class="radio">
  <label>
    <input type="radio" name="optionsRadios" id="optionsRadios1" value="compile" checked>
    Generar codigo para Android Studio o Eclipse
  </label>
</div>
<div class="radio">
  <label>
    <input type="radio" name="optionsRadios" id="optionsRadios2" value="migrate">
    Generar SQL para llenar base de datos
  </label>
</div>
<div class="radio">
  <label>
    <input type="radio" name="optionsRadios" id="optionsRadios2" value="backup">
    Generar SQL para guardar
  </label>
</div>
        <div id = "generador-resultado"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" onclick = "radio_autogenerar_codigo()">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel2">Autogenerar Código > Android Studio o Eclipse > </h4>
      </div>
      <div class="modal-body">
        <button type  ="button" class = "btn btn-primary" id = "boton-autogenerar" onclick="">Generar Código</button>
        <div id = "opcion-extra-top"></div>
        <textarea  id = "codigo-autogenerado" class = "form-control" rows = "25"></textarea>
        <div id = "opcion-extra-bot"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>