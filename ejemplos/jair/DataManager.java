package com.radios.de.bolivia.jair;

import android.content.Context;
import android.widget.TableLayout;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by User on 2014.08.06..
 */
public class DataManager {

    private Context context;
    private String fileName;


    public DataManager(Context context, String fileName) {
        this.context = context;
        this.fileName = fileName;
    }

    public void addNewRadio(String radioListName, String location, String URL) {
        if (loadStoredData() == null) {
            addRadio(radioListName, location, URL, loadStoredData());
        }
        addRadio(radioListName, location, URL, loadStoredData());
    }


    public boolean addRadio(String radioListName, String location, String URL, ArrayList<String> oldLines) {
        try {
            String oldText = "";
            FileOutputStream fOut = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            for (String line : oldLines) {
                String data[] = line.split("<separator>");
                if (data[0].toString().equals(radioListName.toString()))
                    System.out.println("Egyezik");
                else
                    oldText = oldText + "" + line + "\n";
            }
            oldText = oldText + "" + radioListName + "<separator>" + location + "<separator>" + URL;
            fOut.write(oldText.getBytes());
            fOut.close();
            return true;
        } catch (Exception e) {
            e.getMessage();
        }
        return false;
    }

    public boolean deleteExistingData(String radioListName) {
        try {
            ArrayList<String> oldLines = loadStoredData();
            String oldText = "";
            FileOutputStream fOut = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            for (int i = 0; i < oldLines.size(); i++) {
                String part[] = oldLines.get(i).split("<separator>");
                if (!part[0].equals(radioListName) && i < oldLines.size() - 1) {
                    oldText = oldText + "" + oldLines.get(i) + "\n";
                } else if (!part[0].equals(radioListName) && i == oldLines.size() - 1) {
                    oldText = oldText + "" + oldLines.get(i);
                }
            }
            fOut.write(oldText.getBytes());
            fOut.close();
            return true;
        } catch (Exception e) {
            e.getMessage();
        }
        return false;
    }

    public ArrayList<String> loadStoredData() {
        ArrayList<String> lineList = new ArrayList<String>();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(context.openFileInput(fileName)));
            String line;
            while ((line = br.readLine()) != null) {
                lineList.add(line);
            }
            br.close();
            return lineList;
        } catch (Exception e) {
            e.getMessage();
        }
        return null;
    }

    public void loadStoredRadioStations(ArrayList<RadioListElement> radioList, ArrayList<String> userRadios) {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(context.openFileInput(fileName)));
            String line;
            userRadios.clear();
            while ((line = br.readLine()) != null) {
                String data[] = line.split("<separator>");
                System.out.println(line);
                userRadios.add(data[0]);
                radioList.add(new RadioListElement(context, data[0], data[1], data[2], true));
            }
            br.close();
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void createRadioListForRadioScreen(TableLayout UIRadioList,  ArrayList<String> userRadios, TextView radioListName, TextView radioListLocation) {
        ArrayList<RadioListElement> radioArray = new ArrayList<RadioListElement>();
        MainActivity.getDataManager().loadStoredRadioStations(radioArray, userRadios);
        

        radioArray.add(new RadioListElement(context, "Radio Patria Nueva", "", "http://190.129.90.43:8000/;"));
    	radioArray.add(new RadioListElement(context, "Radio Panamericana", "", "http://realserver5.megalink.com:8070/"));
    	radioArray.add(new RadioListElement(context, "Radio Maria", "", "http://dreamsiteradiocp.com:8068/"));
    	radioArray.add(new RadioListElement(context, "Radio Esperanza - Aiquile", "", "http://radios.istbolivia.com:9972/"));
    	radioArray.add(new RadioListElement(context, "Radio Doble 8", "", "http://209.133.216.3:7368/stream"));
    	radioArray.add(new RadioListElement(context, "Radio Pachamama", "", "http://208.85.5.254:7158/;"));
    	radioArray.add(new RadioListElement(context, "EL Sonido De La Vida", "", "http://server.streambolivia.com:9890/;"));
    	radioArray.add(new RadioListElement(context, "Radio Mega DJ", "", "http://eu8.fastcast4u.com:5682/;"));
    	radioArray.add(new RadioListElement(context, "Radio Maritima", "", "http://72.29.70.22:8078/"));
    	radioArray.add(new RadioListElement(context, "Radio Fides la paz", "", "http://192.99.17.12:6358/;"));
    	radioArray.add(new RadioListElement(context, "Erbol FM", "", "http://208.85.5.254:7223/live"));
    	radioArray.add(new RadioListElement(context, "Radio Classica", "", "http://classicafm.scbbs.net:8000/;stream.mp3"));
    	radioArray.add(new RadioListElement(context, "Radio Aclo Tarija", "", "http://185.76.77.113:9938/"));
    	radioArray.add(new RadioListElement(context, "Radio Betania", "", "http://icecast.radiobetania.com:8000/betania.mp3"));
        radioArray.add(new RadioListElement(context, "Radio America 103.6", "", "http://198.100.152.234:8236/"));
    	radioArray.add(new RadioListElement(context, "Radio Disney", "", "http://144.217.67.108:9156/"));
    	radioArray.add(new RadioListElement(context, "Radio Estelar", "", "http://91.121.220.14:9960/;"));
    	radioArray.add(new RadioListElement(context, "Radio Pasion Boliviana", "", "http://realserver2.megalink.com:8140/"));
        radioArray.add(new RadioListElement(context, "Radio Encuentro Sucre", "", "http://zeus.imd.la:6014/;"));
        radioArray.add(new RadioListElement(context, "Radio Aclo - Chuquisaca", "", "http://185.76.77.113:9906/;stream.nsv"));
    	radioArray.add(new RadioListElement(context, "RQP", "", "http://playerservices.streamtheworld.com/api/livestream-redirect/RQPAAC"));
    	radioArray.add(new RadioListElement(context, "Radio Kollasuyo", "", "http://91.121.220.14:9962/stream/1/"));
        radioArray.add(new RadioListElement(context, "Radio Estrella", "", "http://200.58.82.171:8000/;"));  
    	radioArray.add(new RadioListElement(context, "Radio Sucre", "", "http://radios.imd.com.bo:7791/"));
    	radioArray.add(new RadioListElement(context, "Radio Qhana 98.5", "", "http://108.178.57.196:8087/stream"));
    	radioArray.add(new RadioListElement(context, "Radio Camargo", "", "http://aliasdns.info:8778/;")); 
    	radioArray.add(new RadioListElement(context, "El Deber Radio", "", "http://62.210.24.124:8612/live"));   
    	radioArray.add(new RadioListElement(context, "Radio Patrimonio", "", "http://72.29.90.201:9994/;stream.mp3"));
    	radioArray.add(new RadioListElement(context, "Radio Frontera", "", "http://195.154.182.222:26346/popular2"));
    	radioArray.add(new RadioListElement(context, "BBN Bolivia", "", "http://bbnspanish-lh.akamaihd.net/i/BibleBroadcasting_Spanishios@87757/master.m3u8"));
    	radioArray.add(new RadioListElement(context, "Radio Chacaltaya", "", "rtmp://hdpanel.imd.la:1935/chacas/chacas"));
    	radioArray.add(new RadioListElement(context, "Radio Ciudad", "", "http://66.7.222.98:9304/stream/1/"));
    	radioArray.add(new RadioListElement(context, "Radio Aclo - Potosi", "", "http://185.76.77.113:9906/stream/1/"));
    	radioArray.add(new RadioListElement(context, "Radio Caleta", "", "rtsp://media1.cdnlayer.biz:1935/8154/default.stream"));
    	radioArray.add(new RadioListElement(context, "Doble 8 Latina 88.5", "", "http://realserver5.megalink.com:9000/radio_int3.mp3"));
    	radioArray.add(new RadioListElement(context, "Radio La Bruja", "", "http://91.121.220.14:9952/;"));
    	radioArray.add(new RadioListElement(context, "Radio Atl�ntica FM 88.9", "", "rtsp://hdp2.imd.la/atlantica/atlantica"));
    	radioArray.add(new RadioListElement(context, "Radio Uno 89.5 FM", "", "http://radios.tecnosoul.com.ar:8000/radio1"));
    	radioArray.add(new RadioListElement(context, "Integraci�n Montero 90.3", "", "http://62.210.24.124:8339/live"));
    	radioArray.add(new RadioListElement(context, "Radio Audiokiss 90.7 FM", "", "http://radioserver1.us:7040/;"));
    	radioArray.add(new RadioListElement(context, "Radio WKM 91.5 FM", "", "http://radio1.us.mediastre.am/wkmradio.aac"));
        radioArray.add(new RadioListElement(context, "Radio Gigante 94.9", "", "http://stm21.srvstm.com:23712/;"));
        radioArray.add(new RadioListElement(context, "Radio Estrella 93.1 FM", "", "http://200.58.82.171:8000/;"));
    	radioArray.add(new RadioListElement(context, "Radio Exito 93.3 FM", "", "http://stm16.srvstm.com:8460/;"));
    	radioArray.add(new RadioListElement(context, "Radio FmBolivia 101.3 FM", "", "http://72.29.90.201:7002/;stream.mp3"));
    	radioArray.add(new RadioListElement(context, "Radio La Tremenda 95.8 FM", "", "http://radios.bolivianservers.net:8004/;"));
    	radioArray.add(new RadioListElement(context, "Radio Centro 96.3 FM", "", "http://playservistream.net:7172/;"));
    	radioArray.add(new RadioListElement(context, "Radio Andres Iba�ez 97.9 FM", "", "http://ic.streann.com:8000/rai979.ogg"));
    	radioArray.add(new RadioListElement(context, "Radio Mega", "", "http://72.29.70.22:8036/;stream.mp3"));
    	radioArray.add(new RadioListElement(context, "Radio Maritima 99.9 FM", "", "http://72.29.70.22:8078/"));
    	radioArray.add(new RadioListElement(context, "Radio Red Erbol 100.9 FM", "", "http://184.154.45.106:8447/live"));
        radioArray.add(new RadioListElement(context, "Catolica Carisma 103.1 FM", "", "http://72.29.89.35:8036/;"));
    	radioArray.add(new RadioListElement(context, "Radio Deseo 103.3 FM", "", "http://195.154.56.114:9166/;.mp3"));
        radioArray.add(new RadioListElement(context, "El Deber Radio 104.3 FM", "", "http://62.210.24.124:8612/live"));
    	radioArray.add(new RadioListElement(context, "Radio Majestad 105.7 FM", "", "http://stm28.srvstm.com:24214/;"));
    	radioArray.add(new RadioListElement(context, "Classica 107.1 FM", "", "http://classicafm.scbbs.net:8000"));
    	radioArray.add(new RadioListElement(context, "Radio Metropolitana 940", "", "rtsp://hdpanel.imd.la:1935/metroo/metroo"));
    	radioArray.add(new RadioListElement(context, "Radio Bolivianisima Fm", "", "http://67.212.179.132:9900/"));
    	radioArray.add(new RadioListElement(context, "Radio Con Todo Respeto", "", "rtmp://hdpanel.imd.la:1935/qhanass/qhanass"));
    	radioArray.add(new RadioListElement(context, "Radio Splendid Bolvia", "", "http://72.29.90.201:7010/;stream.mp3"));
    	radioArray.add(new RadioListElement(context, "Radio Uchumachi 103.FM", "", "http://boliviastreaming.com:7006/"));
    	radioArray.add(new RadioListElement(context, "Radio Gente 88.9 Fm", "", "http://91.121.220.14:9966"));
    	radioArray.add(new RadioListElement(context, "Radio San Gabriel", "", "http://72.29.70.22:8072/;stream.nsv"));
    	radioArray.add(new RadioListElement(context, "Radio Atl�ntida 102.7 tarija", "", "rtsp://sisteplay1.streamwowza.com:1935/radiohd/Atlantida"));    
        radioArray.add(new RadioListElement(context, "Clasica 100.3 Cochabamba", "", "http://radios.istbolivia.com:9976/"));
        radioArray.add(new RadioListElement(context, "Radio Exito 93.1", "", "http://stm16.srvstm.com:8460/;"));
    	radioArray.add(new RadioListElement(context, "Radio Jacinto Rodriguez", "", "http://192.190.87.194:9402/aac"));
    	radioArray.add(new RadioListElement(context, "Radio Admirable", "", "http://realserver6.megalink.com:9010/radio6.mp3"));
        radioArray.add(new RadioListElement(context, "Radio Kantuta - Oruro", "", "http://45.43.200.82:23128/stream/1/"));
    	radioArray.add(new RadioListElement(context, "Sudamericana FM 100.5", "", "http://suda.coninfo.net:8530/stream"));
        radioArray.add(new RadioListElement(context, "Radio Yungas", "", "http://www.boliviastreaming.com:7052/;"));
        radioArray.add(new RadioListElement(context, "Radio Kawsachun Coca", "", "http://192.190.87.194:9392/stream"));
    	radioArray.add(new RadioListElement(context, "Caliente 97.5", "", "http://stm23.srvstm.com:23324"));
    	radioArray.add(new RadioListElement(context, "Caliente 105.1", "", "http://stream01.net-bolivia.com:9992"));
    	radioArray.add(new RadioListElement(context, "Radio Cadena RCN", "", "http://www.boliviastreaming.com:7002/"));
    	radioArray.add(new RadioListElement(context, "Radio Mix 93.1", "", "http://vps.net-bolivia.com:9996"));
    	radioArray.add(new RadioListElement(context, "La Mix FM", "", "http://37.48.73.96:9996/;"));
       radioArray.add(new RadioListElement(context, "Hit FM 99.1", "", "rtsp://74.222.14.55/16252/16252.stream"));
        radioArray.add(new RadioListElement(context, "San Gabriel 620 AM", "", "http://72.29.70.22:8072/;"));
        radioArray.add(new RadioListElement(context, "Sonido Lider 95.9", "", "http://78.31.64.151:9976/"));
    	radioArray.add(new RadioListElement(context, "Guadalquivir 91.5 - 1420", "", "http://www.boliviastreaming.com:7030"));
        radioArray.add(new RadioListElement(context, "Luis De Fuentes 93.1", "", "http://galohubner.com:7026/;"));
    	radioArray.add(new RadioListElement(context, "Fides loyola", "", "http://192.99.17.12:6358/;"));
    	radioArray.add(new RadioListElement(context, "Pasion Boliviana", "", "http://realserver2.megalink.com:8140/"));
        radioArray.add(new RadioListElement(context, "Radio Soberania", "", "http://195.154.182.222:26215/soberania"));
    	radioArray.add(new RadioListElement(context, "Radio Fides - potosi", "", "http://192.99.17.12:6440/"));
        radioArray.add(new RadioListElement(context, "Radio Bendita Trinidad", "", "http://195.154.182.222:25107/live"));
    	radioArray.add(new RadioListElement(context, "Radio Renovacion Punata", "", "rtsp://hdp2.imd.la:1935/reno/reno"));
    	radioArray.add(new RadioListElement(context, "Stereo 97", "", "http://195.154.182.222:27147/live"));
    	radioArray.add(new RadioListElement(context, "Melod�a FM", "", "http://195.154.182.222:27148/live"));
    	radioArray.add(new RadioListElement(context, "Radio Restauraci�n FM", "", "http://cast.miradio.in:8118/stream"));
    	radioArray.add(new RadioListElement(context, "La Red Deportiva", "", "rtmp://hdp2.imd.la:1935/atb/atb"));
    	radioArray.add(new RadioListElement(context, "Radio Cordillera", "", "http://5.135.183.124:8233/stream"));
    	radioArray.add(new RadioListElement(context, "Radio Activa", "", "http://radioserver1.us:7002/;"));
        radioArray.add(new RadioListElement(context, "Radio Astro", "", "http://radios.istbolivia.com:7899/;"));
    	radioArray.add(new RadioListElement(context, "FM La Paz", "", "http://icecasthd.net:45019/live"));
    	radioArray.add(new RadioListElement(context, "R�dio Bol�via", "", "http://www.boliviastreaming.com:7002/stream/1/"));
    	radioArray.add(new RadioListElement(context, "Radio San Simon", "", "http://stream.systemcyber.com:8148/"));
        radioArray.add(new RadioListElement(context, "Radio Maria Auxiliadora Montero", "", "http://62.210.24.124:8454/live"));
    	radioArray.add(new RadioListElement(context, "Radio Transamericana", "", "http://95.154.254.151:15016/stream/1/"));
    	radioArray.add(new RadioListElement(context, "Sol FM", "", "rtmp://190.181.31.162/live/radio/radio1"));
    	radioArray.add(new RadioListElement(context, "Radio CEPRA Satelital Bolivia", "", "http://192.190.87.194:9396/stream"));
    	radioArray.add(new RadioListElement(context, "Radio Solidaria RKM", "", "rtsp://hdpanel.imd.la:1935/rkm/rkm"));
    	radioArray.add(new RadioListElement(context, "Radio Televisi�n Mundial", "", "http://62.210.24.124:8499/live"));
    	radioArray.add(new RadioListElement(context, "FM Estudiantil UAP", "", "http://70.38.73.27:8129/live"));
    	radioArray.add(new RadioListElement(context, "Radio Rock Boliviano", "", "http://streaming.radionomy.com/Elium-Rock"));
    	radioArray.add(new RadioListElement(context, "Radio Ritmo 97.5", "", "http://tocabox.com/player/proxy.php?station=fama&proxy=true&rnd=1191"));
    	radioArray.add(new RadioListElement(context, "radio caliente cochabamba", "", "http://stm8.srvstm.com:9088/;"));
    	radioArray.add(new RadioListElement(context, "radio Hit de cochabamba", "", "rtsp://sisteplay1.streamwowza.com:1935/radiohd//Hitfm"));
    	radioArray.add(new RadioListElement(context, "radio onda tarije�a", "", "rtsp://sisteplay1.streamwowza.com:1935/radiohd//Onda"));
    	radioArray.add(new RadioListElement(context, "98.5 radio moda", "", "http://199.195.194.140:8156/stream"));
    	radioArray.add(new RadioListElement(context, "ATB Radio", "", "http://66.7.222.98:9308/;"));
    	radioArray.add(new RadioListElement(context, "batallon topater", "", "rtsp://hdp2.imd.la/topater/topater"));
    	radioArray.add(new RadioListElement(context, "camelot hit fm 90.3", "", "http://195.154.182.222:26214/camelot"));
    	radioArray.add(new RadioListElement(context, "ciudad 105.1 fm", "", "http://66.7.222.98:9304/stream/1/"));
    	radioArray.add(new RadioListElement(context, "ciudad 91.3 fm", "", "http://66.7.222.98:9304/"));
    	radioArray.add(new RadioListElement(context, "Classica 106.9", "", "http://classicafm.scbbs.net:8000/"));
    	radioArray.add(new RadioListElement(context, "cristo viene la red 100.6", "", "http://usa1.usastreams.com:8000/cbtsrl"));
    	radioArray.add(new RadioListElement(context, "el sonido de la vida", "", "http://server.streambolivia.com:9890/;"));
    	radioArray.add(new RadioListElement(context, "fm tropical", "", "http://stm46.sibol.org:20026/"));
    	radioArray.add(new RadioListElement(context, "fortaleza 103.5 fm ", "", "http://45.43.200.202:9684/;"));
    	radioArray.add(new RadioListElement(context, "fusion fx 97.6", "", "http://192.190.87.194:9420/stream/1/"));
    	radioArray.add(new RadioListElement(context, "galaxia fm 98", "", "http://iplinea.com:9890/;"));
    	radioArray.add(new RadioListElement(context, "kancha parlaspa 91.9 fm", "", "http://192.190.87.194:9420/stream/1/"));
    	radioArray.add(new RadioListElement(context, "la cruz del sur 720 am", "", "http://38.107.243.219:9006/;"));
    	radioArray.add(new RadioListElement(context, "la nueva estrella 98.7", "", "http://serviprobolivia.com:9854/"));
    	radioArray.add(new RadioListElement(context, "libertad 96.7", "", "rtmp://s2.rtmphd.com:1935/stream1/8308"));
    	radioArray.add(new RadioListElement(context, "nueva pacha 94.5", "", "http://stream.restauraciondefe.com.ar:8000/nuevapacha"));
    	radioArray.add(new RadioListElement(context, "onda tarijenha 103.9 fm", "", "rtsp://sisteplay1.streamwowza.com:1935/radiohd/Onda"));
    	radioArray.add(new RadioListElement(context, "potencia fm 101.2", "", "http://173.249.24.40:8000/stream"));
    	radioArray.add(new RadioListElement(context, "radio 3000 fm", "", "http://centova.mistreaming.com.ve:8458/stream"));
    	radioArray.add(new RadioListElement(context, "activa 91.9", "", "http://radioserver1.us:7002/;"));
    	radioArray.add(new RadioListElement(context, "radio ambana", "", "http://stm13.srvstm.com:14540/;"));
    	radioArray.add(new RadioListElement(context, "radio cabildeo - online", "", "http://www.galohubner.com:7038/;"));
    	radioArray.add(new RadioListElement(context, "compotosi", "", "http://iplinea.com:9880/;"));
    	radioArray.add(new RadioListElement(context, "radio constelacion", "", "http://stm50.srvstm.com:32214/;"));
    	radioArray.add(new RadioListElement(context, "radio del chaco 96.7", "", "rtsp://hdp2.imd.la:1935/chaco/chaco"));
    	radioArray.add(new RadioListElement(context, "radio deseo 103.3", "", "http://195.154.56.114:9166/"));
    	radioArray.add(new RadioListElement(context, "radio ecco cuevo 92.3", "", "http://188.165.236.90:8306/;"));
    	radioArray.add(new RadioListElement(context, "radio hit 105.7", "", "rtsp://sisteplay1.streamwowza.com:1935/radiohd/Hitfm"));
    	radioArray.add(new RadioListElement(context, "radio illampu", "", "http://stm43.sibol.org:8830/;"));
    	radioArray.add(new RadioListElement(context, "radio integracion 650 am", "", "http://stream.nepsum.com:9000/radiointegracion.mp3"));
    	radioArray.add(new RadioListElement(context, "radio integracion 90.3 fm", "", "http://62.210.24.124:8339/live"));
    	radioArray.add(new RadioListElement(context, "radio jhv 93.7 fm ", "", "http://radios.istbolivia.com:9986/stream/1/"));
    	radioArray.add(new RadioListElement(context, " radio lider 97.0 fm", "", "http://stream.radiolider97.bo:8000/;stream.nsv"));
    	radioArray.add(new RadioListElement(context, "radio maya 94.9", "", "http://stm33.srvstm.com:32262/;stream/1"));
    	radioArray.add(new RadioListElement(context, "radio next 91.7", "", "http://212.83.172.242:8183/;"));
    	radioArray.add(new RadioListElement(context, "radio okey", "", "http://ample-zeno-18.radiojar.com/n480zcg5pvduv"));
    	radioArray.add(new RadioListElement(context, "radio play bolivia ", "", "http://162.253.35.125:9300/;"));
    	radioArray.add(new RadioListElement(context, "radio show 95.5", "", "http://stm47.srvstm.com:19116/;"));
    	radioArray.add(new RadioListElement(context, "radio ultrA FM 88.5", "", "http://162.219.28.117:9922/"));
    	radioArray.add(new RadioListElement(context, "radio union dj", "", "http://stm15.srvstm.com:19974/;"));
    	radioArray.add(new RadioListElement(context, "radio uno 96.7", "", "http://198.27.80.34:8020/;"));
    	radioArray.add(new RadioListElement(context, "radio urbana 95.2", "", "http://192.190.87.194:9390/"));
    	radioArray.add(new RadioListElement(context, "radio wara 102.5", "", "http://stm13.srvstm.com:30448/;"));
    	radioArray.add(new RadioListElement(context, "radio yo emito", "", "http://5.199.169.190:8121/stream"));
    	radioArray.add(new RadioListElement(context, "rck-1 99.7 fm ", "", "http://198.27.80.34:6054/;"));
    	radioArray.add(new RadioListElement(context, "red pio XII 97.9", "", "http://aler.org:8000/pio12cbba.mp3"));
    	radioArray.add(new RadioListElement(context, "santa cruz 92.2 fm", "", "http://167.114.64.181:8726/stream"));
    	radioArray.add(new RadioListElement(context, "radio yotala 93.3 fm", "", "http://aliasdns.info:8778/"));
    	radioArray.add(new RadioListElement(context, "radio Silver mix", "", "http://stm28.srvstm.com:14024/;"));
    	radioArray.add(new RadioListElement(context, "RADIO KOLLASUYO DE POTOSi", "", "http://91.121.220.14:9962/stream/1/"));
    	radioArray.add(new RadioListElement(context, "Radio ACLO Tarija", "", "http://185.76.77.113:9906/"));
    	radioArray.add(new RadioListElement(context, "Radio Guadalquivir 91.5 FM Tarija", "", "http://server.streambolivia.com:9310/stream/1/"));
    	radioArray.add(new RadioListElement(context, "Luis de Fuentes", "", "rtmp://hdpanel.imd.la:1935/fuentes/fuentes"));
    	radioArray.add(new RadioListElement(context, "Radio Kollasuyo", "", "http://91.121.220.14:9962/"));
    	radioArray.add(new RadioListElement(context, "radio metropolitana 940 am", "", "rtmp://hdpanel.imd.la:1935/metroo/metroo"));  
    	
    	
        UIRadioList.removeAllViews();
        RadioList radioList = new RadioList(context, radioArray, UIRadioList);
        radioList.addRadioStations(radioListName, radioListLocation);

    }

}





















