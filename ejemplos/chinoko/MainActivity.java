package com.emisoras.bolivia.xino;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.emisoras.bolivia.xino.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import io.vov.vitamio.MediaPlayer;
import io.vov.vitamio.Vitamio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

@SuppressLint("ClickableViewAccessibility") public class MainActivity extends FragmentActivity {

    private static DataManager dataManager;
    private static TableLayout UIRadioList;
    private static ArrayList<String> userRadios = new ArrayList<String>();
    private static ViewPager viewPager;
    private static ImageView bufferingIndicator, speaker, startOrStopBtn;
    private static LoadingAnimation bufferingAnimation;
    private static AudioManager audioManager;
    private static TextView radioListLocation, radioListName, radioTitle;
    private ImageView screenChaneButton, plus;
    private boolean runOnce = true;
    private LinearLayout volumeLayout, volumeButton;
    private int volumeStore;
    private ImageView previousBtn, nextBtn;
    private AdView adView;
    private Typeface fontRegular;
    private static EditText editBusqueda;
    
    private static ArrayList<RadioListElement> radioArray = new ArrayList<RadioListElement>();
    private static ArrayList<RadioListElement> radioArrayFiltrado = new ArrayList<RadioListElement>();
    
    private MediaPlayer mPlayer = null;
    
    public static void radioListRefresh() {
        dataManager.createRadioListForRadioScreen(UIRadioList, userRadios, radioListName, radioListLocation, radioArrayFiltrado);
    }

    public static void setViewPagerSwitch() {
        viewPager.setCurrentItem(0, true);
    }

    public static void startBufferingAnimation() {
        bufferingIndicator = MainScreen.getLoadingImage();
        bufferingAnimation = new LoadingAnimation(bufferingIndicator);
        bufferingAnimation.startAnimation();
    }

    public static void stopBufferingAnimation() {
        bufferingIndicator = MainScreen.getLoadingImage();
        bufferingAnimation.clearAnimation();
    }

    public static DataManager getDataManager() {
        return dataManager;
    }

    public static ArrayList<String> getUserRadios() {
        return userRadios;
    }

    public static TextView getRadioListLocation() {
        return radioListLocation;
    }

    public static ImageView getStartOrStopBtn() {
        return startOrStopBtn;
    }

    public static void nextPage() {
        viewPager.setCurrentItem(1, true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        
       
        Vitamio.isInitialized(this);
        //streamAudio("rtsp://sisteplay1.streamwowza.com:1935/radiohd/vivalima");

        dataManager = new DataManager(this, "user_radio");
        fontRegular = Typeface.createFromAsset(getAssets(), "fonts/font.otf");
        radioTitle = (TextView) findViewById(R.id.radioTitle);
        
        radioTitle.setTypeface(fontRegular);

        plus = (ImageView) findViewById(R.id.plus);
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(viewPager.getCurrentItem()==1){
                    final Dialog dialog = new Dialog(MainActivity.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.add_radio_dialog);
                    FrameLayout mainLayout = (FrameLayout) findViewById(R.id.mainLayout);
                    dialog.getWindow().setLayout((int) (mainLayout.getWidth() * 0.8), (int) (mainLayout.getHeight() * 0.4));
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                    final EditText radioListName = (EditText) dialog.getWindow().findViewById(R.id.dialogradioListName);
                    final EditText radioUrl = (EditText) dialog.getWindow().findViewById(R.id.dialogRadioUrl);
                    TextView dialogTitle = (TextView) dialog.getWindow().findViewById(R.id.dialogTitle);
                    Button confirmBtn = (Button) dialog.getWindow().findViewById(R.id.confirm);
                    Button cancelBtn = (Button) dialog.getWindow().findViewById(R.id.cancel);

                    dialogTitle.setTypeface(fontRegular);
                    radioListName.setTypeface(fontRegular);
                    radioUrl.setTypeface(fontRegular);
                    confirmBtn.setTypeface(fontRegular);
                    cancelBtn.setTypeface(fontRegular);

                    radioUrl.addTextChangedListener(new TextWatcher() {
                        private boolean isInAfterTextChanged;
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                        }

                        @Override
                        public void afterTextChanged(Editable editable) {
                            if (!isInAfterTextChanged) {
                                isInAfterTextChanged = true;
                                if(radioUrl.getText().toString().length()==1){
                                    radioUrl.setText("http://"+editable.toString());
                                    radioUrl.setSelection(radioUrl.getText().length());
                                }else if(radioUrl.getText().toString().length()<7){
                                    radioUrl.setText("http://");
                                    radioUrl.setSelection(radioUrl.getText().length());
                                }
                                else if(!radioUrl.getText().toString().contains("http://")){
                                    radioUrl.setText("http://"+editable.toString());
                                    radioUrl.setSelection(radioUrl.getText().length());
                                }else if(radioUrl.getText().toString().contains("http://")){
                                    String split[]=radioUrl.getText().toString().split("http://");
                                    if(split.length>2){
                                        radioUrl.setText("http://"+radioUrl.getText().toString().replace("http://", ""));
                                        radioUrl.setSelection(radioUrl.getText().length());
                                    }
                                }
                                if(radioUrl.getCurrentTextColor()==Color.RED){
                                    radioUrl.setTextColor(Color.parseColor("#FFAAAAAA"));
                                }
                                isInAfterTextChanged = false;
                            }
                        }
                    });

                    confirmBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (radioListName.getText().length() != 0 && radioUrl.getText().length() != 0) {
                                if(radioUrl.getText().toString().endsWith(".pls") || radioUrl.getText().toString().endsWith(".m3u")){
                                    try {
                                        String url=new FileToUrl().execute(radioUrl.getText().toString()).get();
                                        if(url.toString()=="empty")
                                            radioUrl.setTextColor(Color.RED);
                                        else{
                                            dataManager.addNewRadio(radioListName.getText().toString(), "Own station", url);
                                            radioListRefresh();
                                            dialog.dismiss();
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }else if(radioUrl.getText().toString().endsWith(".asx"))
                                    radioUrl.setTextColor(Color.RED);
                                else{
                                    dataManager.addNewRadio(radioListName.getText().toString(), "Own station", radioUrl.getText().toString());
                                    radioListRefresh();
                                    dialog.dismiss();
                                }
                            }
                        }
                    });

                    cancelBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
                else System.exit(0);
            }
        });


        TabPagerAdapter tabPageAdapter = new TabPagerAdapter(getSupportFragmentManager());
        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(tabPageAdapter);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {
            }

            @Override
            public void onPageSelected(int i) {
                if (viewPager.getCurrentItem() == 0) {
                    radioTitle.setText("Online");
                    radioTitle.setTextColor(Color.parseColor("#99FF00"));
                    plus.setImageResource(R.drawable.exit);
                    screenChaneButton.setImageResource(R.drawable.switch_page);
                    adView.setVisibility(View.VISIBLE);
                } else {
                    radioTitle.setText("Radios");
                    radioTitle.setTextColor(Color.parseColor("#FFFFFF"));
                    plus.setImageResource(R.drawable.plus);
                    screenChaneButton.setImageResource(R.drawable.back);
                    if(Boolean.parseBoolean(getResources().getString(R.string.admob_true_or_false)))
                        adView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });

        screenChaneButton = (ImageView) findViewById(R.id.nextScreen);
        screenChaneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (viewPager.getCurrentItem() == 0) viewPager.setCurrentItem(1, true);
                else viewPager.setCurrentItem(0, true);
            }
        });
        speaker = (ImageView) findViewById(R.id.speaker);
        speaker.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) == 0) {
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volumeStore, volumeStore);
                    defaultVolumeBarPosition(audioManager, volumeLayout, volumeButton);
                } else {
                    volumeStore = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
                    defaultVolumeBarPosition(audioManager, volumeLayout, volumeButton);
                }
                return false;
            }
        });
        volumeLayout = (LinearLayout) findViewById(R.id.linearLayout_t);
        volumeButton = (LinearLayout) findViewById(R.id.button_t);
        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);

        registerReceiver(new HeadsetReceiver(getApplicationContext()), new IntentFilter(Intent.ACTION_HEADSET_PLUG));
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(this.TELEPHONY_SERVICE);
        telephonyManager.listen(new PhoneCallListener(), PhoneStateListener.LISTEN_CALL_STATE);

        previousBtn = (ImageView) findViewById(R.id.previous_btn);
        nextBtn = (ImageView) findViewById(R.id.next_btn);
        startOrStopBtn = (ImageView) findViewById(R.id.start_or_stop_btn);

        
        
        
        adView = (AdView) this.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        adView.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
            defaultVolumeBarPosition(audioManager, volumeLayout, volumeButton);
        else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP)
            defaultVolumeBarPosition(audioManager, volumeLayout, volumeButton);
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)
            defaultVolumeBarPosition(audioManager, volumeLayout, volumeButton);
        else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP)
            defaultVolumeBarPosition(audioManager, volumeLayout, volumeButton);
        else if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        // TODO Auto-generated method stub
        super.onWindowFocusChanged(hasFocus);
        if (runOnce) {
            UIRadioList = (TableLayout) findViewById(R.id.radioListUi);
            radioListName = (TextView) findViewById(R.id.mainRadioName);
            radioListLocation = (TextView) findViewById(R.id.mainRadioLocation);
            startWallpaperAnimation();
            radioListRefresh();
            volumeBarReaction(volumeLayout, volumeButton, audioManager);
            new MusicPlayerControl(previousBtn, startOrStopBtn, nextBtn, radioListLocation, radioListName).setOnTouchListeners();
            connectionDialog(isOnline());
            FrameLayout mainLayout = (FrameLayout) findViewById(R.id.mainLayout);
            RadioList.declarateDialog(this, (int) (mainLayout.getWidth() * 0.8), (int) (mainLayout.getHeight() * 0.3));
            runOnce = false;

            
            
            //////////////////////////////////////////////////////////////////////////////////////////////
			//////////////////////////////////////////////////////////////////////////////////////////////
			editBusqueda = (EditText)findViewById(R.id.editBusqueda);
			
			generarRadios();
			radioArrayFiltrado.clear();
			radioArrayFiltrado.addAll(radioArray);
			radioListRefresh();
			
			editBusqueda.addTextChangedListener(new TextWatcher() {
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {}
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,int after) {}
				@Override
				public void afterTextChanged(Editable s) {
					if(!editBusqueda.getText().toString().trim().equals("")){
						filtrarRadios(editBusqueda.getText().toString());
					}else{
						radioArrayFiltrado.clear();
						radioArrayFiltrado.addAll(radioArray);
						radioListRefresh();
					}
				}
			});
			
			//////////////////////////////////////////////////////////////////////////////////////////////
			//////////////////////////////////////////////////////////////////////////////////////////////
        }
        defaultVolumeBarPosition(audioManager, volumeLayout, volumeButton);
    }

    private void startWallpaperAnimation() {
        Point size = new Point();
        Display display = getWindowManager().getDefaultDisplay();
        display.getSize(size);
        LinearLayout picture = (LinearLayout) findViewById(R.id.wallpaper);
        TranslateAnimation animation = new TranslateAnimation(0, 0 - (picture.getWidth() - size.x), 0, 0);
        animation.setInterpolator(new LinearInterpolator());
        animation.setDuration(200000);
        animation.setFillAfter(true);
        animation.setRepeatMode(Animation.REVERSE);
        animation.setRepeatCount(Animation.INFINITE);
        picture.startAnimation(animation);
    }

    public void defaultVolumeBarPosition(AudioManager audioManager, LinearLayout volumeLayout, LinearLayout volumeButton) {
        float actual = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        float max = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        float endPoint = volumeLayout.getWidth() - volumeButton.getWidth();
        volumeButton.setX((endPoint / max * actual));
        if (volumeButton.getX() == 0)
            speaker.setImageResource(R.drawable.volume_muted);
        else speaker.setImageResource(R.drawable.parlante);
    }

    @SuppressLint("ClickableViewAccessibility") public void volumeBarReaction(final LinearLayout volumeLayout, final LinearLayout volumeButton, final AudioManager audioManager) {

        volumeLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                float max = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
                float endPoint = volumeLayout.getWidth() - volumeButton.getWidth();
                volumeButton.setX(motionEvent.getX()-volumeButton.getWidth()/2);

                if (volumeButton.getX() >= 0) {
                    float pos = volumeButton.getX() / (endPoint / max);
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, (int) pos, 0);
                }
                if (volumeButton.getX() >= endPoint) {
                    volumeButton.setX(endPoint);
                }
                if (volumeButton.getX() <= 0) {
                    volumeButton.setX(0);
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
                    speaker.setImageResource(R.drawable.volume_muted);
                }
                else speaker.setImageResource(R.drawable.parlante);
                return true;
            }
        });
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        else return false;
    }

    public void connectionDialog(boolean isOnline){
        if(!isOnline){
            final Dialog dialog = new Dialog(MainActivity.this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.without_internet);
            FrameLayout mainLayout = (FrameLayout) findViewById(R.id.mainLayout);
            dialog.getWindow().setLayout((int) (mainLayout.getWidth() * 0.8), (int) (mainLayout.getHeight() * 0.45));
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            Button retryBtn = (Button) dialog.getWindow().findViewById(R.id.retry);
            retryBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!isOnline()){
                        dialog.dismiss();
                        dialog.show();
                    }else{
                        dialog.dismiss();
                        radioListRefresh();
                    }
                }
            });
            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    if(!isOnline()){
                        dialog.dismiss();
                        dialog.show();
                    }
                }
            });
            dialog.show();
    }
    }

    public class FileToUrl extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... strings) {
            String radioURL = "empty";
            try {
                URL url = new URL(strings[0]);
                URLConnection uc = url.openConnection();
                BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    System.out.println(inputLine);
                    if (inputLine.contains("http://")) {
                        String[] fields = inputLine.split("http://");
                        radioURL = "http://" + fields[1];
                        in.close();
                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return radioURL;
        }
    }
    
    private void filtrarRadios(String texto){
    	radioArrayFiltrado.clear();
    	for(RadioListElement r : radioArray){
    		if(r.getName().toLowerCase().indexOf(texto.toLowerCase()) != -1){
    			radioArrayFiltrado.add(r);
    		}
    	}
    	radioListRefresh();
    }
    private void generarRadios(){
    	radioArray.add(new RadioListElement(this, "Radio Patria Nueva", "", "http://190.129.90.43:8000/;",R.drawable.emi01));
    	radioArray.add(new RadioListElement(this, "Radio Panamericana", "", "http://realserver5.megalink.com:8070/",R.drawable.emi02));
    	radioArray.add(new RadioListElement(this, "Radio Maria", "", "http://dreamsiteradiocp4.com:8022/;",R.drawable.emi03));
    	radioArray.add(new RadioListElement(this, "Radio Esperanza - Aiquile", "", "http://107.170.95.131:9940/;stream.mp3",R.drawable.emi04));
    	radioArray.add(new RadioListElement(this, "Radio Doble 8", "", "http://realserver5.megalink.com:9000/radio_int3.mp3",R.drawable.emi05));
    	radioArray.add(new RadioListElement(this, "Radio Pachamama", "", "http://184.154.45.106:8011/;stream.mp3",R.drawable.emi06));
    	radioArray.add(new RadioListElement(this, "EL Sonido De La Vida", "", "http://server.streambolivia.com:9890/;",R.drawable.emi07));
    	radioArray.add(new RadioListElement(this, "Radio Mega DJ", "", "http://eu8.fastcast4u.com:5682/;",R.drawable.emi08));
    	radioArray.add(new RadioListElement(this, "Radio Maritima", "", "http://72.29.70.22:8078/",R.drawable.emi09));
    	radioArray.add(new RadioListElement(this, "Radio Fides la paz", "", "http://192.99.17.12:6358/;",R.drawable.emi10));
    	radioArray.add(new RadioListElement(this, "Erbol FM", "", "http://184.154.45.106:8447/live",R.drawable.emi11));
    	radioArray.add(new RadioListElement(this, "Radio Classica", "", "http://classicafm.scbbs.net:8000/;",R.drawable.emi12));
    	radioArray.add(new RadioListElement(this, "Radio Aclo Tarija", "", "http://185.76.77.113:9906/",R.drawable.emi13));
    	radioArray.add(new RadioListElement(this, "Radio Betania", "", "http://icecast.radiobetania.com:8000/betania.mp3",R.drawable.emi14));
        radioArray.add(new RadioListElement(this, "Radio America 103.6", "", "http://198.100.152.234:8236/",R.drawable.emi15));
    	radioArray.add(new RadioListElement(this, "Radio Disney", "", "http://144.217.67.108:9156/",R.drawable.emi16));
    	radioArray.add(new RadioListElement(this, "Radio Estelar", "", "http://91.121.220.14:9960/;",R.drawable.emi17));
    	radioArray.add(new RadioListElement(this, "Radio Pasion Boliviana", "", "http://realserver2.megalink.com:8140/;",R.drawable.emi18));
    	radioArray.add(new RadioListElement(this, "Radio Encuentro Sucre", "", "http://zeus.imd.la:6014/;",R.drawable.emi19));
    	radioArray.add(new RadioListElement(this, "Radio Aclo - Chuquisaca", "", "http://185.76.77.113:9938/",R.drawable.emi20));
    	radioArray.add(new RadioListElement(this, "RQP", "", "http://playerservices.streamtheworld.com/api/livestream-redirect/RQPAAC",R.drawable.emi21));
    	radioArray.add(new RadioListElement(this, "Radio Kollasuyo", "", "http://91.121.220.14:9962/stream/1/",R.drawable.emi22));
    	radioArray.add(new RadioListElement(this, "Radio Estrella", "", "http://200.58.82.171:8000/;",R.drawable.emi23));  
    	radioArray.add(new RadioListElement(this, "Radio Sucre", "", "http://radios.imd.com.bo:7791/",R.drawable.emi24));
        radioArray.add(new RadioListElement(this, "Radio Qhana 98.5", "", "http://108.163.229.102:8156/",R.drawable.emi25));
    	radioArray.add(new RadioListElement(this, "Radio Camargo", "", "http://192.99.99.1:8778/;",R.drawable.emi26));  
    	radioArray.add(new RadioListElement(this, "El Deber Radio", "", "http://62.210.24.124:8612/live",R.drawable.emi27));   
    	radioArray.add(new RadioListElement(this, "Radio Patrimonio", "", "http://72.29.90.201:9994/;stream.mp3",R.drawable.emi28));
    	radioArray.add(new RadioListElement(this, "Radio Frontera", "", "http://173.244.209.219:8094/frontera1?",R.drawable.emi29));
    	radioArray.add(new RadioListElement(this, "BBN Bolivia", "", "http://bbnspanish-lh.akamaihd.net/i/BibleBroadcasting_Spanishios@87757/master.m3u8",R.drawable.emi30));
    	radioArray.add(new RadioListElement(this, "Radio Chacaltaya", "", "http://realserver2.megalink.com:8110/;",R.drawable.emi31));
    	radioArray.add(new RadioListElement(this, "Radio Ciudad", "", "http://66.7.222.98:9304/stream/1/",R.drawable.emi32));
    	radioArray.add(new RadioListElement(this, "Radio Aclo - Potosi", "", "http://185.76.77.113:9952/;",R.drawable.emi33));
    	radioArray.add(new RadioListElement(this, "Radio Caleta", "", "rtsp://media1.cdnlayer.biz:1935/8154/default.stream",R.drawable.emi34));
    	radioArray.add(new RadioListElement(this, "Doble 8 Latina 88.5", "", "http://realserver5.megalink.com:9000/radio_int3.mp3",R.drawable.emi35));
    	radioArray.add(new RadioListElement(this, "Radio La Bruja", "", "http://labruja.loweserver.es:8000/stream2",R.drawable.emi36));
    	radioArray.add(new RadioListElement(this, "Radio Atl�ntica FM 88.9", "", "rtsp://hdp2.imd.la/atlantica/atlantica",R.drawable.emi37));
    	radioArray.add(new RadioListElement(this, "Radio Uno 89.5 FM", "", "http://radios.tecnosoul.com.ar:8000/radio1",R.drawable.emi38));
    	radioArray.add(new RadioListElement(this, "Integraci�n Montero 90.3", "", "http://5.199.169.190:8119/live",R.drawable.emi39));
    	radioArray.add(new RadioListElement(this, "Radio Audiokiss 90.7 FM", "", "http://radioserver1.us:7040/;",R.drawable.emi40));
    	radioArray.add(new RadioListElement(this, "Radio WKM 91.5 FM", "", "http://radio1.us.mediastre.am/wkmradio.aac?type=.flv",R.drawable.emi41));
    	radioArray.add(new RadioListElement(this, "Radio Gigante 94.9", "", "rtsp://rtmp1.srvstm.com/23712/23712.stream",R.drawable.emi42));
        radioArray.add(new RadioListElement(this, "Radio Estrella 93.1 FM", "", "http://200.58.82.171:8000/;",R.drawable.emi43));
    	radioArray.add(new RadioListElement(this, "Radio Exito 93.3 FM", "", "http://162.253.35.125:9302",R.drawable.emi44));
    	radioArray.add(new RadioListElement(this, "Radio FmBolivia 101.3 FM", "", "http://72.29.90.201:7002/;stream.mp3",R.drawable.emi45));
    	radioArray.add(new RadioListElement(this, "Radio La Tremenda 95.8 FM", "", "http://radios.bolivianservers.net:8004/;stream.aac",R.drawable.emi46));
    	radioArray.add(new RadioListElement(this, "Radio Centro 96.3 FM", "", "http://playservistream.net:7172/;",R.drawable.emi47));
    	radioArray.add(new RadioListElement(this, "Radio Andres Iba�ez 97.9 FM", "", "http://72.29.70.22:8198/;",R.drawable.emi48));
    	radioArray.add(new RadioListElement(this, "Radio Mega", "", "http://198.204.234.12:9990/",R.drawable.emi49));
    	radioArray.add(new RadioListElement(this, "Radio Maritima 99.9 FM", "", "http://72.29.70.22:8078/;",R.drawable.emi50));
    	radioArray.add(new RadioListElement(this, "Radio Red Erbol 100.9 FM", "", "http://184.154.45.106:8447/live",R.drawable.emi51));
    	radioArray.add(new RadioListElement(this, "Catolica Carisma 103.1 FM", "", "http://72.29.89.35:8036/;",R.drawable.emi52));
    	radioArray.add(new RadioListElement(this, "Radio Deseo 103.3 FM", "", "rtsp://stream.dattastreaming.com/streamaac/9166",R.drawable.emi53));
    	radioArray.add(new RadioListElement(this, "El Deber Radio 104.3 FM", "", "http://62.210.24.124:8340/live",R.drawable.emi54));
    	radioArray.add(new RadioListElement(this, "Radio Majestad 105.7 FM", "", "http://stm28.srvstm.com:24214/;",R.drawable.emi55));
    	radioArray.add(new RadioListElement(this, "Classica 107.1 FM", "", "http://classicafm.scbbs.net:8000",R.drawable.emi56));
    	radioArray.add(new RadioListElement(this, "Radio Metropolitana 940", "", "http://72.29.90.201:7022/;stream.aac",R.drawable.emi57));
    	radioArray.add(new RadioListElement(this, "Radio Bolivianisima Fm", "", "http://67.212.179.132:9900/;listen.mp3",R.drawable.emi58));
    	radioArray.add(new RadioListElement(this, "Radio Con Todo Respeto", "", "rtmp://hdpanel.imd.la:1935/qhanass/qhanass",R.drawable.emi59));
    	radioArray.add(new RadioListElement(this, "Radio Splendid Bolvia", "", "http://72.29.90.201:7010/;stream.mp3",R.drawable.emi60));
    	radioArray.add(new RadioListElement(this, "Radio Uchumachi 103.FM", "", "http://boliviastreaming.com:7006/",R.drawable.emi61));
    	radioArray.add(new RadioListElement(this, "Radio Gente 88.9 Fm", "", "http://91.121.220.14:9966/",R.drawable.emi62));
    	radioArray.add(new RadioListElement(this, "Radio San Gabriel", "", "http://72.29.70.22:8072/;stream.nsv",R.drawable.emi63));
    	radioArray.add(new RadioListElement(this, "Radio Atl�ntida 102.7 tarija", "", "rtsp://sisteplay1.streamwowza.com:1935/radiohd/Atlantida",R.drawable.emi64));    
    	radioArray.add(new RadioListElement(this, "Clasica 100.3 Cochabamba", "", "http://radios.istbolivia.com:9976/;?_=0.7462371088441169",R.drawable.emi65));
    	radioArray.add(new RadioListElement(this, "Radio Exitosa 104.2", "", "http://boliviastreaming.com:7042",R.drawable.emi66));  
    	radioArray.add(new RadioListElement(this, "Radio Jacinto Rodriguez", "", "http://192.190.87.194:9402/aac",R.drawable.emi67));
    	radioArray.add(new RadioListElement(this, "Radio Admirable", "", "http://realserver6.megalink.com:9010/radio6.mp3",R.drawable.emi68));
    	radioArray.add(new RadioListElement(this, "Radio Kantuta - Oruro", "", "http://45.43.200.82:23128/stream/1/",R.drawable.emi69));
    	radioArray.add(new RadioListElement(this, "Sudamericana FM 100.5", "", "http://suda.coninfo.net:8530/stream",R.drawable.emi70));
    	radioArray.add(new RadioListElement(this, "Radio Yungas", "", "http://www.boliviastreaming.com:7052/;",R.drawable.emi71));
    	radioArray.add(new RadioListElement(this, "Radio Kawsachun Coca", "", "http://5.199.169.190:8098/stream",R.drawable.emi72));
    	radioArray.add(new RadioListElement(this, "Caliente 97.5", "", "http://stm23.srvstm.com:23324/;",R.drawable.emi73));
    	radioArray.add(new RadioListElement(this, "Caliente 105.1", "", "http://stream01.net-bolivia.com:9992",R.drawable.emi74));
    	radioArray.add(new RadioListElement(this, "Radio Cadena RCN", "", "http://www.boliviastreaming.com:7002/",R.drawable.emi75));
    	radioArray.add(new RadioListElement(this, "Radio Mix 93.1", "", "http://37.48.73.96:9996/stream/1/",R.drawable.emi76));
    	radioArray.add(new RadioListElement(this, "La Mix FM", "", "http://aliasdns.info:8102/live",R.drawable.emi77));
        radioArray.add(new RadioListElement(this, "Hit FM 99.1", "", "rtsp://74.222.14.55/16252/16252.stream",R.drawable.emi78));
    	radioArray.add(new RadioListElement(this, "San Gabriel 620 AM", "", "http://72.29.70.22:8072/",R.drawable.emi79));
    	radioArray.add(new RadioListElement(this, "Sonido Lider 95.9", "", "http://198.204.234.12:9976/",R.drawable.emi80));
    	radioArray.add(new RadioListElement(this, "Guadalquivir 91.5 - 1420", "", "http://www.boliviastreaming.com:7030",R.drawable.emi81));
    	radioArray.add(new RadioListElement(this, "Luis De Fuentes 93.1", "", "rtsp://boliviastreaming.com:1935/fuentes/fuentes",R.drawable.emi82));
    	radioArray.add(new RadioListElement(this, "Fides loyola", "", "http://192.99.17.12:6358/;",R.drawable.emi83));
    	radioArray.add(new RadioListElement(this, "Pasion Boliviana", "", "http://realserver2.megalink.com:8140/;",R.drawable.emi84));
        radioArray.add(new RadioListElement(this, "Radio Soberania", "", "http://195.154.182.222:26215/soberania",R.drawable.emi85));
    	radioArray.add(new RadioListElement(this, "Radio Fides - potosi", "", "http://192.99.17.12:6358/;",R.drawable.emi86));
        radioArray.add(new RadioListElement(this, "Radio Bendita Trinidad", "", "http://195.154.217.185:9970/;stream.mp3",R.drawable.emi87));
    	radioArray.add(new RadioListElement(this, "Radio Renovacion Punata", "", "rtsp://hdp2.imd.la:1935/reno/reno",R.drawable.emi88));
    	radioArray.add(new RadioListElement(this, "Stereo 97", "", "http://195.154.182.222:27147/live",R.drawable.emi89));
    	radioArray.add(new RadioListElement(this, "Melod�a FM", "", "http://195.154.182.222:27148/live",R.drawable.emi90));
    	radioArray.add(new RadioListElement(this, "Radio Restauraci�n FM", "", "http://cast.miradio.in:8118/stream",R.drawable.emi91));
    	radioArray.add(new RadioListElement(this, "La Red Deportiva", "", "http://66.7.222.98:9308/stream",R.drawable.emi92));
    	radioArray.add(new RadioListElement(this, "Radio Cordillera", "", "http://5.135.183.124:8233/stream",R.drawable.emi93));
    	radioArray.add(new RadioListElement(this, "Radio Activa", "", "http://streamserver3.us:7050/stream?type=.mp3",R.drawable.emi94));
    	radioArray.add(new RadioListElement(this, "Radio Astro", "", "http://radios.istbolivia.com:7899/;",R.drawable.emi95));
    	radioArray.add(new RadioListElement(this, "FM La Paz", "", "http://icecasthd.net:45019/live",R.drawable.emi96));
    	radioArray.add(new RadioListElement(this, "R�dio Bol�via", "", "rtsp://hdpanel.imd.la:1935/fmbolivia/fmbolivia",R.drawable.emi97));
    	radioArray.add(new RadioListElement(this, "Radio San Simon", "", "http://stream.systemcyber.com:8148/;",R.drawable.emi98));
    	radioArray.add(new RadioListElement(this, "Radio Maria Auxiliadora Montero", "", "http://dreamsiteradiocp.com:8068/",R.drawable.emi99));
    	radioArray.add(new RadioListElement(this, "Radio Transamericana", "", "http://87.117.228.65:28964/",R.drawable.emi100));
    	radioArray.add(new RadioListElement(this, "Sol FM", "", "rtmp://190.181.31.162/live/radio/radio1",R.drawable.emi101));
    	radioArray.add(new RadioListElement(this, "Radio CEPRA Satelital Bolivia", "", "http://5.199.169.190:8319/cepra",R.drawable.emi102));
    	radioArray.add(new RadioListElement(this, "Radio Solidaria RKM", "", "rtmp://hdpanel.imd.la:1935/rkm/rkm",R.drawable.emi103));
    	radioArray.add(new RadioListElement(this, "Radio Televisi�n Mundial", "", "http://5.199.169.190:8036/live",R.drawable.emi104));
    	radioArray.add(new RadioListElement(this, "FM Estudiantil UAP", "", "http://streaming.jargon.com.ar:10000/;",R.drawable.emi105));
    	radioArray.add(new RadioListElement(this, "Radio Rock Boliviano", "", "http://streaming.radionomy.com/Elium-Rock",R.drawable.emi106));
    	radioArray.add(new RadioListElement(this, "Radio Ritmo 97.5", "", "http://tocabox.com/player/proxy.php?station=fama&proxy=true&rnd=1191",R.drawable.emi107));
    	radioArray.add(new RadioListElement(this, "radio aclo chuquisa", "", "http://185.76.77.113:9906/",R.drawable.emi108));
    	radioArray.add(new RadioListElement(this, "radio cultura", "", "http://190.129.73.203:8000/",R.drawable.emi109));
    	radioArray.add(new RadioListElement(this, "radio caliente cochabamba", "", "http://stm8.srvstm.com:9088/;",R.drawable.emi110));
    	radioArray.add(new RadioListElement(this, "radio Hit - cochabamba", "", "rtsp://sisteplay1.streamwowza.com:1935/radiohd//Hitfm",R.drawable.emi111));
    	radioArray.add(new RadioListElement(this, "radio onda tarije�a", "", "rtsp://sisteplay1.streamwowza.com:1935/radiohd//Onda",R.drawable.emi112));
    	radioArray.add(new RadioListElement(this, "radio activa", "", "http://radioserver1.us:7002/;",R.drawable.emi113));
    	radioArray.add(new RadioListElement(this, "98.5 radio moda", "", "http://199.195.194.140:8156/stream",R.drawable.emi114));
    	radioArray.add(new RadioListElement(this, "ATB Radio", "", "http://66.7.222.98:9308/;",R.drawable.emi115));
    	radioArray.add(new RadioListElement(this, "batallon topater", "", "rtsp://hdp2.imd.la:1935/topater/topater",R.drawable.emi116));
    	radioArray.add(new RadioListElement(this, "camelot hit fm 90.3", "", "http://195.154.182.222:26214/camelot",R.drawable.emi117));
    	radioArray.add(new RadioListElement(this, "ciudad 105.1 fm", "", "http://66.7.222.98:9304/",R.drawable.emi118));
    	radioArray.add(new RadioListElement(this, "ciudad 91.3 fm", "", "http://66.7.222.98:9304/stream/1/",R.drawable.emi119));
    	radioArray.add(new RadioListElement(this, "Classica 106.9", "", "http://classicafm.scbbs.net:8000/",R.drawable.emi120));
    	radioArray.add(new RadioListElement(this, "cristo viene la red 100.6", "", "http://cdn.akausa240.usastreams.com:9322/;",R.drawable.emi121));
    	radioArray.add(new RadioListElement(this, "el sonido de la vida", "", "http://server.streambolivia.com:9890/;",R.drawable.emi122));
    	radioArray.add(new RadioListElement(this, "fm tropical", "", "http://173.208.235.238:9322/;",R.drawable.emi123));
    	radioArray.add(new RadioListElement(this, "fortaleza 103.5 fm", "", "rtsp://74.222.14.55/9684/9684.stream",R.drawable.emi124));
    	radioArray.add(new RadioListElement(this, "fusion fx 97.6", "", "http://170.75.144.250:21394/",R.drawable.emi125));
    	radioArray.add(new RadioListElement(this, "galaxia fm 98", "", "http://iplinea.com:9890/;",R.drawable.emi126));
    	radioArray.add(new RadioListElement(this, "kancha parlaspa 91.9 fm", "", "http://5.199.169.190:8251/stream",R.drawable.emi127));
    	radioArray.add(new RadioListElement(this, "la cruz del sur 720 am", "", "http://38.107.243.219:9006/;",R.drawable.emi128));
    	radioArray.add(new RadioListElement(this, "la nueva estrella 98.7", "", "http://serviprobolivia.com:9854/;",R.drawable.emi129));
    	radioArray.add(new RadioListElement(this, "libertad 96.7", "", "rtsp://s2.rtmphd.com:1935/stream1/8308",R.drawable.emi130));
    	radioArray.add(new RadioListElement(this, "nueva pacha 94.5", "", "http://stream.restauraciondefe.com.ar:8000/nuevapacha",R.drawable.emi131));
    	radioArray.add(new RadioListElement(this, "onda tarijenha 103.9 fm", "", "rtsp://sisteplay1.streamwowza.com:1935/radiohd/Onda",R.drawable.emi132));
    	radioArray.add(new RadioListElement(this, "potencia fm 101.2", "", "http://65.60.62.181:9822/;",R.drawable.emi133));
    	radioArray.add(new RadioListElement(this, "radio 3000 fm", "", "http://centova.mistreaming.com.ve:8458/stream",R.drawable.emi134));
    	radioArray.add(new RadioListElement(this, "activa 91.9", "", "http://streamserver3.us:7050/stream?type=.mp3",R.drawable.emi135));
    	radioArray.add(new RadioListElement(this, "radio ambana", "", "http://stm13.srvstm.com:14540/;",R.drawable.emi136));
    	radioArray.add(new RadioListElement(this, "radio cabildeo - online ", "", "http://108.178.57.196:8037/;",R.drawable.emi137));
    	radioArray.add(new RadioListElement(this, "compotosi", "", "http://iplinea.com:9880/;",R.drawable.emi138));
    	radioArray.add(new RadioListElement(this, "radio constelacion", "", "http://stm5.srvstm.com:18920/;",R.drawable.emi139));
    	radioArray.add(new RadioListElement(this, "radio del chaco 96.7", "", "rtsp://hdp2.imd.la:1935/chaco/chaco",R.drawable.emi140));
    	radioArray.add(new RadioListElement(this, "radio deseo 103.3", "", "rtsp://stream.dattastreaming.com:1935/streamaac/9166",R.drawable.emi141));
    	radioArray.add(new RadioListElement(this, "radio ecco cuevo 92.3", "", "http://188.165.236.90:8306/;",R.drawable.emi142));
    	radioArray.add(new RadioListElement(this, "radio hit 105.7", "", "rtsp://sisteplay1.streamwowza.com:1935/radiohd/Hitfm",R.drawable.emi143));
    	radioArray.add(new RadioListElement(this, "radio illampu", "", "http://stm43.sibol.org:8830/;",R.drawable.emi144));
    	radioArray.add(new RadioListElement(this, "radio integracion 650 am", "", "http://62.210.24.124:8339/live",R.drawable.emi145));
    	radioArray.add(new RadioListElement(this, "radio integracion 90.3 fm", "", "http://5.199.169.190:8119/live",R.drawable.emi146));
    	radioArray.add(new RadioListElement(this, "radio jhv 93.7 fm ", "", "http://radios.istbolivia.com:9986/stream/1/",R.drawable.emi147));
    	radioArray.add(new RadioListElement(this, " radio lider 97.0 fm", "", "http://stream.radiolider97.bo:8000/;stream.nsv ",R.drawable.emi148));
    	radioArray.add(new RadioListElement(this, "radio maya 94.9", "", "http://199.233.233.42:25648/",R.drawable.emi149));
    	radioArray.add(new RadioListElement(this, "radio next 91.7", "", "http://212.83.172.242:8183/;",R.drawable.emi150));
    	radioArray.add(new RadioListElement(this, "radio okey", "", "http://45.43.200.106:21218/;",R.drawable.emi151));
    	radioArray.add(new RadioListElement(this, "radio play bolivia ", "", "http://162.253.35.125:9300/;",R.drawable.emi152));
    	radioArray.add(new RadioListElement(this, "radio show 95.5", "", "http://stm47.srvstm.com:19116/;",R.drawable.emi153));
    	radioArray.add(new RadioListElement(this, "radio ultrA FM 88.5", "", "http://162.219.28.117:9922/;",R.drawable.emi154));
    	radioArray.add(new RadioListElement(this, "radio union  dj", "", "http://stm15.srvstm.com:19974/;",R.drawable.emi155));
    	radioArray.add(new RadioListElement(this, "radio uno 96.7", "", "http://198.27.80.34:8020/;",R.drawable.emi156));
    	radioArray.add(new RadioListElement(this, "radio urbana 95.2", "", "http://5.199.169.190:8121/stream?1493402798512",R.drawable.emi157));
    	radioArray.add(new RadioListElement(this, "radio wara 102.5", "", "http://stm10.sibol.org:8850/;",R.drawable.emi158));
    	radioArray.add(new RadioListElement(this, "radio yo emito", "", "http://5.199.169.190:8121/stream",R.drawable.emi159));
    	radioArray.add(new RadioListElement(this, "rck-1 99.7 fm ", "", "http://198.27.80.34:6054/;",R.drawable.emi160));
    	radioArray.add(new RadioListElement(this, "red pio XII 97.9", "", "http://aler.org:8000/pio12cbba.mp3",R.drawable.emi161));
    	radioArray.add(new RadioListElement(this, "santa cruz 92.2 fm", "", "http://s36.myradiostream.com:10250/;stream.mp3",R.drawable.emi162));
    	radioArray.add(new RadioListElement(this, "radio yotala 93.3 fm", "", "http://162.253.35.125:9302/;",R.drawable.emi163));
    	

    	
    }
    
}


