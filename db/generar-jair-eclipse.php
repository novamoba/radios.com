<?php
	include "net.php";

	$resp = array("estado"=>false);

	$tabla = $_GET['tabla'];

	$sql = "SELECT nombre, url FROM {$tabla} WHERE activo = 1";

	$query = $mysqli->query($sql);

	$gen = "";

	while($r = $query->fetch_assoc()) {
		$resp['estado'] = true;
		ob_start();
		?>
		radioArray.add(new RadioListElement(context, "<?= $r['nombre'] ?>", "", "<?= $r['url'] ?>"));
		<?php
		$gen .= ob_get_clean();
		
	}

	$resp['gen'] = $gen;

	echo json_encode($resp);

	