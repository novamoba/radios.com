<?php

	include "net.php";

	$resp = array("estado"=>false);

	$tabla = $_GET['country'];

	$id = $_GET['id'];

	$sql = "SELECT * FROM {$tabla} WHERE id = {$id}";

	$query = $mysqli->query($sql);

	if ($r = $query->fetch_assoc()) {
		$resp['estado'] = true;
		$resp['nombre'] = $r['nombre'];
		$resp['web'] = $r['web'];
		$resp['url'] = $r['url'];
		$resp['imagen'] = $r['imagen'];
		$resp['notas'] = $r['notas'];
		$resp['lastUpdate'] = $r['last_update'];
	}

	echo json_encode($resp);


?>