<?php

	include "net.php";

	$respuesta = array("respuesta" => "");

	$tabla = $_GET['tabla'];

	$id = $_GET['id'];

	$activo = $_GET['activo'];

	if ($activo == 1) {
		$activo = 0;
		ob_start();
		?>
		<button type="button" id = "suiche-<?= $id ?>" class="btn btn-danger" onclick = "radio_suiche(<?= $id ?>, <?= $activo ?>)"><span class = "glyphicon glyphicon-remove"></span></button>
		<?php
		$respuesta['respuesta'] = ob_get_clean();
	} else {
		$activo = 1;
		ob_start();
		?>
		<button type="button" id = "suiche-<?= $id ?>" class="btn btn-success" onclick = "radio_suiche(<?= $id ?>, <?= $activo ?>)"><span class = "glyphicon glyphicon-ok"></span></button>
		<?php
		$respuesta['respuesta'] = ob_get_clean();
	}

	$sql = "UPDATE {$tabla} SET activo = {$activo} WHERE id = {$id}";

	$mysqli->query($sql);

	echo json_encode($respuesta);


	