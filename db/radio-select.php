<?php

	function radio_element($nombre, $url, $id, $estado, $web) {
		ob_start();
		?>
		<form class="form-inline go-bottom" style = "padding-top: 5px;">
		  <div class="form-group col-xs-1" align="center" >
		  	<?= $id ?>
		  </div>
		  <div class="form-group">
		    
		    <input type="text" class="form-control input-quick" id="inputName-<?= $id ?>" placeholder="Radio NombrE" value = "<?= $nombre ?>" required>
		    <label for="inputName-<?= $id ?>">Nombre</label>
		  </div>

		  <div class="form-group">
		    
		    <input style = "width: 270px;" type="text" class="form-control input-quick" id="inputUrl-<?= $id ?>" placeholder="http://url.url" value = "<?= $url ?>" onfocus = "$(this).select();" required>
		    <label for="inputUrl-<?= $id ?>">URL</label>
		  </div>
		  <?php if ($estado == 1) { ?>
		  <button type="button" id = "suiche-<?= $id ?>" class="btn btn-success" onclick = "radio_suiche(<?= $id ?>, <?= $estado ?>)"><span class = "glyphicon glyphicon-ok"></span></button>
		  <?php } else { ?>
		  <button type="button" id = "suiche-<?= $id ?>" class="btn btn-danger" onclick = "radio_suiche(<?= $id ?>, <?= $estado ?>)"><span class = "glyphicon glyphicon-remove"></span></button>
		  <?php } ?>
		  <button type="button" class="btn btn-default" onclick = "radio_formEditar(<?= $id ?>)"><span class = "glyphicon glyphicon-pencil"></span></button>
		  <button type="button" class="btn btn-default" onclick="radio_openWeb('<?= $web ?>')"><span class = "glyphicon glyphicon-globe"></span></button>
		  <button type="button" class="btn btn-default" onclick="radio_quickSave(<?= $id ?>)"><span class = "glyphicon glyphicon-floppy-disk"></span></button>
		  <button type="button" class="btn btn-default" onclick = "radio_manualTester(<?= $id ?>)"><span class = "glyphicon glyphicon-eye-open"></span></button>
		  <button type="button" class="btn btn-default" id = "botonPlay-<?= $id ?>" onclick = "radio_play(<?= $id ?>)"><span class = "glyphicon glyphicon-play"></span></button>
		  
		  <span id = "status-<?= $id ?>"></span>
		</form>
		<?php
		return ob_get_clean();
	}

	$tabla = $_GET['tabla'];

	include "net.php";

	$sql = "SELECT id, nombre, url, activo, web FROM {$tabla}";


	$query = $mysqli->query($sql);

	$contador = 0;
	while($r = $query->fetch_assoc()) {
		$contador++;
		echo radio_element($r['nombre'], $r['url'], $r['id'], $r['activo'], $r['web']);
	}

	?>
	<script>
		todas = <?= $contador ?>;
	</script>

