
  var country = "honduras";
  var radios = [];
  var todas = 0;
  var actual = 0;

  function radio_manualTester(ide) {
        var sURL = $("#inputUrl-"+ide).val();
        console.log(sURL);
        //if (soundManager.canPlayURL(sURL)) {
          //alert("Si podemos correrlo!")
            var mySoundObject = soundManager.createSound({
             url: sURL,
             onplay: function() {
              console.log("onplay ejecutdo!") 
              $("#status-"+ide).html("<i class = 'ic ic-loading'></i>");
            },
            onerror: function() {
              console.log("onerror ejecutado!") // ejecutado efectivamente al haber error
              $("#status-"+ide).html("<span style = 'color:red;' class = 'glyphicon glyphicon-remove'></span>");
              var tryToFixUrl = fixUrl(sURL);
              if (tryToFixUrl != sURL) {
                  radio_tester2(tryToFixUrl, ide);
              }

            },
            whileplaying: function() {
              console.log("whileplaying fue ejecutado") // ejecutado cuando el sonido corre
              //soundManager.stop(mySoundObject.getSoundById());
              $("#status-"+ide).html("<span style = 'color:lime;' class = 'glyphicon glyphicon-ok'></span>");
              mySoundObject.stop();
            }
            });


           mySoundObject.play(); // SMSound object instance
  }

  function openModal_radioNueva() {
      $('#myModal').modal('show');
      //$('#myModal').modal('hide');
      restablecer_modal()
  }

  function restablecer_modal() {
      $("#inputNombre").val("");
      $("#inputUrl").val("");
      $("#inputImagen").val("");
      $("#inputWeb").val("");
      $("#inputNotas").val("");
      $("#last-update").html("");
      $("#botonModal0").attr("onclick", "radio_guardar()")
  }

  function radio_quickSave(ide) {
      var nombre = $("#inputName-"+ide).val();
      var url = $("#inputUrl-"+ide).val();

      $.ajax({
        url: "db/radio-quick-update.php",
        data:{
          tabla:country,
          id:ide,
          nombre:nombre,
          url:url
        },
        success: function(r) {
            var json = JSON.parse(r);
            if (json.estado == true) {
                alert("Radio actualizada.");
            }
        }
      });
  }

  function radio_quickUpdate(id, nombre, url, web) {
      $("#inputName-"+id).val(nombre)
      $("#inputUrl-"+id).val(url)
  }

  function radio_play(ide) {
      var sURL = $("#inputUrl-"+ide).val();
      $("#botonPlay-"+ide).html("<span class='glyphicon glyphicon-pause'></span>");
      $("#botonPlay-"+ide).attr("onclick", "radio_stop("+ide+")")
        console.log(sURL);
        //if (soundManager.canPlayURL(sURL)) {
          //alert("Si podemos correrlo!")

          if ((sURL.search("rtmp") != -1) || (sURL.search("rtsp") != -1)) {

          }

            radios[ide] = soundManager.createSound({
             url: sURL,
             onplay: function() {
              console.log("onplay ejecutdo!") 
              $("#status-"+ide).html("<i class = 'ic ic-loading'></i>");
            },
            onerror: function() {
              console.log("onerror ejecutado!") // ejecutado efectivamente al haber error
              $("#status-"+ide).html("<span style = 'color:red;' class = 'glyphicon glyphicon-remove'></span>");
            },
            whileplaying: function() {
              console.log("whileplaying fue ejecutado") // ejecutado cuando el sonido corre
              //soundManager.stop(mySoundObject.getSoundById());
              $("#status-"+ide).html("<span style = 'color:lime;' class = 'glyphicon glyphicon-ok'></span>");
              //mySoundObject.stop();
            }
            });


           radios[ide].play(); // SMSound object instance
  }

  function radio_stop(ide) {
      radios[ide].stop();
      $("#botonPlay-"+ide).html("<span class='glyphicon glyphicon-play'></span>");
      $("#botonPlay-"+ide).attr("onclick", "radio_play("+ide+")")
  }

  function RadioTestAll() {
      if (todas == 0) {
        alert("No hay radios para probar.")
        return;
      }

      tRadio_test(1)

  }

  function tRadio_test(ide) {
      var sURL = $("#inputUrl-"+ide).val();
        console.log(sURL);
        //if (soundManager.canPlayURL(sURL)) {
          //alert("Si podemos correrlo!")
          if (sURL == undefined) {
            alert("Se han probado todas las RADIOS presentes.")
            return;
          }
            var mySoundObject = soundManager.createSound({
             url: sURL,
             onplay: function() {
              console.log("onplay ejecutdo!") 
              $("#status-"+ide).html("<i class = 'ic ic-loading'></i>");
            },
            onerror: function() {
              console.log("onerror ejecutado!") // ejecutado efectivamente al haber error
              $("#status-"+ide).html("<span style = 'color:red;' class = 'glyphicon glyphicon-remove'></span>");
              var tryToFixUrl = fixUrl(sURL);
              if (tryToFixUrl != sURL) {
                  tRadio_test2(tryToFixUrl, ide);
              } 
              else
              tRadio_test(ide+1)
            },
            whileplaying: function() {
              console.log("whileplaying fue ejecutado") // ejecutado cuando el sonido corre
              //soundManager.stop(mySoundObject.getSoundById());
              $("#status-"+ide).html("<span style = 'color:lime;' class = 'glyphicon glyphicon-ok'></span>");
              mySoundObject.stop();
              mySoundObject.unload();

              tRadio_test(ide+1);
            }
            });

           mySoundObject.play(); // SMSound object instance
  }

  function tRadio_test2(url, ide) {
        var sURL = url;//$("#inputUrl-"+ide).val();
        console.log(sURL);
        //if (soundManager.canPlayURL(sURL)) {
          //alert("Si podemos correrlo!")
          
            var mySoundObject = soundManager.createSound({
             url: sURL,
             onplay: function() {
              console.log("onplay ejecutdo!") 
              $("#status-"+ide).html("<i class = 'ic ic-loading'></i>");
            },
            onerror: function() {
              console.log("onerror ejecutado!") // ejecutado efectivamente al haber error
              $("#status-"+ide).html("<span style = 'color:red;' class = 'glyphicon glyphicon-remove'></span>");
              tRadio_test(ide+1)
            },
            whileplaying: function() {
              console.log("whileplaying fue ejecutado") // ejecutado cuando el sonido corre
              //soundManager.stop(mySoundObject.getSoundById());
              $("#status-"+ide).html("<span style = 'color:lime;' class = 'glyphicon glyphicon-wrench'></span><span style = 'color:lime;' class = 'glyphicon glyphicon-ok'></span>");
              mySoundObject.stop();
              mySoundObject.unload();
              $("#inputUrl-"+ide).val(sURL);
              radio_quickSave(ide)
              tRadio_test(ide+1);
            }
            });

           mySoundObject.play(); // SMSound object instance
  }

  function radio_tester2(url, ide) {
        var sURL = url;//$("#inputUrl-"+ide).val();
        console.log(sURL);
        //if (soundManager.canPlayURL(sURL)) {
          //alert("Si podemos correrlo!")
          
            var mySoundObject = soundManager.createSound({
             url: sURL,
             onplay: function() {
              console.log("onplay ejecutdo!") 
              $("#status-"+ide).html("<i class = 'ic ic-loading'></i>");
            },
            onerror: function() {
              console.log("onerror ejecutado!") // ejecutado efectivamente al haber error
              $("#status-"+ide).html("<span style = 'color:red;' class = 'glyphicon glyphicon-remove'></span>");              
            },
            whileplaying: function() {
              console.log("whileplaying fue ejecutado") // ejecutado cuando el sonido corre
              //soundManager.stop(mySoundObject.getSoundById());
              $("#status-"+ide).html("<span style = 'color:lime;' class = 'glyphicon glyphicon-wrench'></span><span style = 'color:lime;' class = 'glyphicon glyphicon-ok'></span>");
              mySoundObject.stop();
              mySoundObject.unload();    
              $("#inputUrl-"+ide).val(sURL);
              radio_quickSave(ide)      
            }
            });

           mySoundObject.play(); // SMSound object instance
  }



  function fixUrl(url) {
      if (url.substr(url.length - 1) == "/") {
          url += ";";
          console.log("REPARANDO URL para volver a PROBAR por SEGUNDA VEZ....")
      }
      if ((url.indexOf("streamtheworld") !== -1) && (url.indexOf("playerservices") === -1)) {
          var aurl = url.split("/");
          url = "http://playerservices.streamtheworld.com/api/livestream-redirect/" + aurl[aurl.length-1];
          console.log("REPARANDO URL para volver a PROBAR por SEGUNDA VEZ....")
      } 
      switch (url.substr(url.length - 1)) {
          case '0':
          case '1':
          case '2':
          case '3':
          case '4':
          case '5':
          case '6':
          case '7':
          case '8':
          case '9':
            console.log("REPARANDO URL para volver a PROBAR por SEGUNDA VEZ....")
            url += "/;";
          break;
      }
      console.log(url)
      return url;
  }

    $(document).ready(function(){

      radio_mostrar("honduras")
      /*
        $(".manual-check-button").click(function(){
            radio_tester($("#inputUrl-0").val(), "0")
        });
        */
        /*
         //soundManager.createSound('https://ice42.securenetsystems.net/SATELITE');
         // off http://78.129.224.19:19043/;stream.mp3
         var sURL = 'https://ice42.securenetsystems.net/SATELITE';
        //if (soundManager.canPlayURL(sURL)) {
          //alert("Si podemos correrlo!")
            var mySoundObject = soundManager.createSound({
             url: sURL,
             onplay: function() {
              console.log("onplay ejecutdo!") 
            },
            onerror: function() {
              console.log("onerror ejecutado!") // ejecutado efectivamente al haber error
            },
            whileplaying: function() {
              console.log("whileplaying fue ejecutado") // ejecutado cuando el sonido corre
              //soundManager.stop(mySoundObject.getSoundById());
              mySoundObject.stop();
            }
            });


           mySoundObject.play(); // SMSound object instance

            */

             
        //}
                
    });

    function radio_guardar() {
        var nombre = $("#inputNombre").val();
        var url = $("#inputUrl").val();
        var imagen = $("#inputImagen").val();
        var web = $("#inputWeb").val();

        $.ajax({
          url: "db/radio-insert.php",
          data: {
            nombre:nombre,
            url:url,
            imagen:imagen,
            web:web,
            tabla:country
          },
          success: function(r) {
             var json = JSON.parse(r);

             if (json.id > 0) {
                radio_recargar();
                $('#myModal').modal('hide');
                restablecer_modal()
             }
          }
        })
    }

    function radio_recargar() {
        $.ajax({
          url: "db/radio-select.php",
          data: {
            tabla:country
          },
          success: function(r) {
             $("#radio-list").html(r);
          }
        })
    }

    function radio_formEditar(id) {
        
        $.ajax({
          url:"db/radio-select-one.php",
          data:{
            country:country,
            id:id
          },
          success: function(r) {
            var json = JSON.parse(r);
            if (json.estado == true) {
              $("#inputNombre").val(json.nombre)
              $("#inputUrl").val(json.url)
              $("#inputWeb").val(json.web)
              $("#inputImagen").val(json.imagen)
              $("#inputNotas").val(json.notas)
              $("#last-update").html(json.lastUpdate)
              $('#myModal').modal('show');
              $("#botonModal0").attr("onclick", "radio_actualizar("+id+")");
            }

          }
        })

        

    }

    function radio_actualizar(id) {

      var nombre = $("#inputNombre").val();
      var url = $("#inputUrl").val();
      var web = $("#inputWeb").val();
      var imagen = $("#inputImagen").val();
      var notas = $("#inputNotas").val();

      $.ajax({
        url: "db/radio-update-one.php",
        data:{
          id:id,
          tabla:country,
          nombre:nombre,
          url:url,
          web:web,
          imagen:imagen,
          notas:notas
        },
        success: function(r) {
          var json = JSON.parse(r);
          if (json.estado == true) {
            $("#myModal").modal("hide");
            restablecer_modal()
            radio_quickUpdate(id, nombre, url, web)
            alert("Radio actualizada");
          }
        }
      })
    }

    function radio_mostrar(pais) {
        $("."+country).removeClass("active");
        country = pais;
        radio_recargar();
        $("."+pais).addClass("active");
    }

    function radio_suiche(id, bol) {
        $.ajax({
          url: "db/radio-suiche.php",
          data:{
            tabla:country,
            id:id,
            activo:bol
          },
          success: function(r) {
            var json = JSON.parse(r);
            $("#suiche-"+id).replaceWith(json.respuesta)
          }
        })
    }

    function radio_NuevoPais() {
        $('#myModal0').modal('show');
    }

    function radio_generador() {
        $('#myModal1').modal('show');
    }

    function radio_grupoGuardar() {
        var nombre = $("#inputNombreGrupo").val();

        $.ajax({
          url: "db/country-create.php",
          data:{
            nombre:nombre
          },
          success: function(r) {
              var json =  JSON.parse(r);
              if (json.estado == true) {
                radio_grupo_recargar()
                alert("Grupo creado!")
              } else console.log(r)

              $('#myModal0').modal('hide');
          }
        })
    }

    function radio_grupo_recargar() {
      $.ajax({
        url: "db/country-select.php",
        success: function(r) {
           $("#country-list").html(r)
        }
      })
    }

    function radio_autogenerar_codigo() {
        var cuenta = $("#selector-cuenta").val();
        var funcion = "";
        var titulo = "Autogenerar Código ";
        var instrucciones = "";

        var tipo = $('input[name=optionsRadios]:checked', '#form-generar').val();

        if (tipo == "compile") {
          instrucciones = "El código aqui generado debe ser pegado en "
        switch(cuenta) {
          case "Chinoko":
            funcion = "generar_codigo_chinoko()";
            titulo += "> Eclipse > Chinoko"; 
            instrucciones += "el proyecto de eclipse mas especificamente en el archivo MainActivity.java"
          break;
          case "Pao":
            funcion = "generar_codigo_pao()";
            titulo += "> Android Studio > Pao"
            instrucciones += "el proyecto de Android Studio en el archivo string.xml"
          break;
          case "Jair":
            funcion = "generar_codigo_jair()";
            titulo += "> Eclipse > Jair"
            instrucciones += "el proyecto de eclipse mas especificamente en el archivo DataManager.java"
          break;
          case "Caleta.tk":
            funcion = "generar_codigo_caleta()";
            titulo += "> Android Studio > Caleta.tk"
            instrucciones += "el proyecto de Android Studio en el archivo string.xml"
          break;
        }

      } 

      if (tipo == "migrate") {
          var titulo = "Autogenerar SQL ";
          instrucciones = "Debes pegar el código ya existente en ";
          switch(cuenta) {
          case "Chinoko":
            funcion = "generar_sql_chinoko()";
            titulo += "> Eclipse > Chinoko"; 
            instrucciones += "el proyecto de eclipse del archivo MainActivity.java, no todo el código, solo el que contenga las radios y su información. Muy importante: Siempre hay que quitar los comentarios."
          break;
          case "Pao":
            funcion = "generar_sql_pao()";
            titulo += "> Android Studio > Pao";
            instrucciones += "el proyecto de Android Studio del archivo string.xml, solo debe ir la parte de los item (con la información de las radios), no mas, no menos. Muy importante: No deben tener los comentarios, ni la numeración."
          break;
          case "Jair":
            funcion = "generar_sql_jair()";
            titulo += "> Eclipse > Jair"
            instrucciones += "el proyecto de eclipse del archivo DataManager.java, solo debe ir la parte de las radios, sin comentarios, ni nada mas."
          break;
          case "Caleta.tk":
            funcion = "generar_sql_caleta()";
            titulo += "> Android Studio > Caleta.tk"
            instrucciones += "el proyecto de Android Studio del archivo string.xml, solo debe tener los items, sin los comentarios, ni la numeración."
          break;
        }
      }

        


        $("#codigo-autogenerado").val("")
        $("#boton-autogenerar").attr("onclick", funcion)
        $("#myModalLabel2").html(titulo)
        $("#myModal1").modal("hide")
        $("#myModal2").modal("show")
        $("#opcion-extra-top").html(instrucciones)

    }

    function generar_codigo_chinoko() {
        $.ajax({
          url: "db/generar-chinoko-eclipse.php",
          data:{
            tabla:country
          },
          success: function(r) {
            var json = JSON.parse(r);
            if (json.estado == true) {
              $("#codigo-autogenerado").val(json.gen)
            }
          }
        })
    }

    function generar_codigo_jair() {
        $.ajax({
          url: "db/generar-jair-eclipse.php",
          data:{
            tabla:country
          },
          success: function(r) {
            var json = JSON.parse(r);
            if (json.estado == true) {
              $("#codigo-autogenerado").val(json.gen)
            }
          }
        })
    }

    function generar_codigo_caleta() {
        $.ajax({
          url: "db/generar-caletatk-android-studio.php",
          data:{
            tabla:country
          },
          success: function(r) {
            var json = JSON.parse(r);
            if (json.estado == true) {
              $("#codigo-autogenerado").val(json.gen)
            }
          }
        })
    }

    function generar_codigo_pao() {
        $.ajax({
          url: "db/generar-pao-android-studio.php",
          data:{
            tabla:country
          },
          success: function(r) {
            var json = JSON.parse(r);
            if (json.estado == true) {
              $("#codigo-autogenerado").val(json.gen)
            }
          }
        })
    }

    function generar_sql_jair() {
        var codigo = $("#codigo-autogenerado").val();
          codigo = codigo.replace(/radioArray.add\(new RadioListElement\(context, "/g, "INSERT INTO "+country+" (nombre, url) VALUES ('");
          // ", "", "
          codigo = codigo.replace(/", "", "/g, "' , '");
          //codigo = codigo.replace(/",R.drawable./g, "', '");
          codigo = codigo.replace(/"\)\);/g, "' \);");

           $("#codigo-autogenerado").val(codigo); 

           $.ajax({
              url: "db/country-select-elements-generador-form.php",
              success: function(r) {
                     $("#opcion-extra-bot").html(r)
              }
           });
    }

     function generar_sql_caleta() {
        var codigo = $("#codigo-autogenerado").val();
          codigo = codigo.replace(/<item>/g, "INSERT INTO "+country+" (nombre, url, imagen) VALUES ('");
          // ", "", "
          codigo = codigo.replace(/-_-/g, "' , '");
          codigo = codigo.replace(/;a/g, "', 'a");
          codigo = codigo.replace(/<\/item>/g, "' \);");

           $("#codigo-autogenerado").val(codigo); 

           $.ajax({
              url: "db/country-select-elements-generador-form.php",
              success: function(r) {
                     $("#opcion-extra-bot").html(r)
              }
           });
    }

     function generar_sql_pao() {
        var codigo = $("#codigo-autogenerado").val();
          codigo = codigo.replace(/<item>/g, "INSERT INTO "+country+" (nombre, url, imagen) VALUES ('");
          // ", "", "
          codigo = codigo.replace(/-_-/g, "' , '");
          codigo = codigo.replace(/;a/g, "', 'a");
          codigo = codigo.replace(/<\/item>/g, "' \);");

           $("#codigo-autogenerado").val(codigo); 

           $.ajax({
              url: "db/country-select-elements-generador-form.php",
              success: function(r) {
                     $("#opcion-extra-bot").html(r)
              }
           });
    }

     function generar_sql_chinoko() {
          var codigo = $("#codigo-autogenerado").val();
          codigo = codigo.replace(/radioArray.add\(new RadioListElement\(this, "/g, "INSERT INTO "+country+" (nombre, url, imagen) VALUES ('");
          // ", "", "
          codigo = codigo.replace(/", "", "/g, "' , '");
          codigo = codigo.replace(/",R.drawable./g, "', '");
          codigo = codigo.replace(/\)\);/g, "' \);");

           $("#codigo-autogenerado").val(codigo); 

           $.ajax({
              url: "db/country-select-elements-generador-form.php",
              success: function(r) {
                     $("#opcion-extra-bot").html(r)
              }
           });


      }

      function radio_openWeb(url) {
        if (url != "") window.open(url)
          else alert("Error: El campo WEB esta vacío.")
      }